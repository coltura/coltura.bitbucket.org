(function (define) {
    'use strict';

    define([
        'text!../../templates/nav.html',
        'bootstrap'
    ], function (indexTemplate) {

        var vendorDir = 'bower_components/';

        return {
            url: '/home',
            template: indexTemplate
        };
    });
}(this.define))
